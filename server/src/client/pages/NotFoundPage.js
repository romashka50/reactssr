import React from 'react';

const NotFound = ({ staticContext = {} }) => {
  staticContext.notFound = true;

  return (
    <h1>
      Oops, Page not Found
    </h1>
  );
};

export default {
  component: NotFound,
}
