import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUsers } from '../actions/index';
import { Helmet } from 'react-helmet';

class UsersList extends Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  renderUsers() {
    return this.props.users.map(user => (<li key={user.id} >{user.name}</li >));
  }

  head() {
    return (
      <Helmet>
        <title>{`${this.props.users.length} were loaded`}</title>
        <meta property="og:title" content="User App"/>
      </Helmet>
    )
  }

  render() {
    return (
      <div >
        Users:
        {this.head()}
        {this.renderUsers()}
      </div >
    );
  }
}

function loadData(store) {
  return store.dispatch(fetchUsers());
}

const mapStateToProps = ({ users }) => ({
  users,
});

export default {
  component: connect(mapStateToProps, { fetchUsers, })(UsersList),
  loadData,
};
