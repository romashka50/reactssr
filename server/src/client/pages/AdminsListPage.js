import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchAdmins } from '../actions/index';
import requireAuth from '../components/hocs/RequireAuth';

class AdminsList extends Component {
  componentDidMount() {
    this.props.fetchAdmins();
  }

  renderAdmins() {
    return this.props.admins.map(admin => (<li key={admin.id} >{admin.name}</li >));
  }

  render() {
    return (
      <div >
        Secret admins:
        {this.renderAdmins()}
      </div >
    );
  }
}

function loadData(store) {
  return store.dispatch(fetchAdmins());
}

const mapStateToProps = ({ admins }) => ({
  admins,
});

export default {
  component: connect(mapStateToProps, { fetchAdmins, })(requireAuth(AdminsList)),
  loadData,
};
