export const FETCH_USERS = 'FETCH:USERS';
export const FETCH_ADMINS = 'FETCH:ADMINS';
export const FETCH_CURRENT_USERS = 'FETCH:CURRENT:USERS';

export const fetchUsers = () => async (dispatch, getState, api) => {
  const url = '/users';
  const resp = await api.get(url);

  dispatch({
    type: FETCH_USERS,
    payload: resp,
  });
};
export const fetchAdmins = () => async (dispatch, getState, api) => {
  const url = '/admins';
  const resp = await api.get(url);

  dispatch({
    type: FETCH_ADMINS,
    payload: resp,
  });
};
export const fetchCurrentUsers = () => async (dispatch, getState, api) => {
  const url = '/current_user';
  const resp = await api.get(url);

  dispatch({
    type: FETCH_CURRENT_USERS,
    payload: resp,
  });
};
