// import 'babel-polyfill'; initially was in course. I replaced it with appropriate entry in webpack.server.js
import express from 'express';
import proxy from 'express-http-proxy';
import { matchRoutes } from 'react-router-config';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';
import routes from './client/Routes';

const app = express();

app.use('/api', proxy('http://react-ssr-api.herokuapp.com', {
  proxyReqOptDecorator(opts) {
    opts.headers['x-forwarded-host'] = 'localhost:3000';

    return opts;
  }
}));
app.use(express.static('public'));
app.get('*', (req, res) => {
  const store = createStore(req);
  const matches = matchRoutes(routes, req.path);
  const promises = matches.map(({ route }) => {
    return route.loadData ? route.loadData(store) : null
  }).map((promise) => {
    if (promise) {
      return new Promise((resolve) => {
        promise.then(resolve).catch(resolve);
      });
    }
  });

  Promise.all(promises).then(() => {
    const context = {};
    const content = renderer(req, store, context);

    let status = 200;
    if (context.url) {
      return res.redirect(301, context.url);
    }
    if (context.notFound) {
      status = 404;
    }

    res.status(status).send(content);
  });
});

app.listen(3000, () => {
  console.log('Listening on port 3000');
});
